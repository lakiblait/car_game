import pygame   # импортируем библиотеку pygame (подключили ее)

pygame.init()   # инициализируем библиотеку pygame (запустили ее)
win = pygame.display.set_mode((500, 475))   # создаем наше окно (характеристики его) (размер)

pygame.display.set_caption("Car Game")  # создаем заголовок нашему окну

# указываем путь к картинкам для движений влево, вправо, стоять на месте и т.д
walkRight = [pygame.image.load('car2.png'),
             pygame.image.load('car2.png'), pygame.image.load('car2.png'),
             pygame.image.load('car2.png'), pygame.image.load('car2.png'),
             pygame.image.load('car2.png'), pygame.image.load('car2.png'),
             pygame.image.load('car2.png'), pygame.image.load('car2.png'),
             pygame.image.load('car2.png')]

walkLeft = [pygame.image.load('car1.png'),
            pygame.image.load('car1.png'), pygame.image.load('car1.png'),
            pygame.image.load('car1.png'), pygame.image.load('car1.png'),
            pygame.image.load('car1.png'), pygame.image.load('car1.png'),
            pygame.image.load('car1.png'), pygame.image.load('car1.png'),
            pygame.image.load('car1.png')]

# walkRight = [pygame.image.load('Walk000.png'),
#              pygame.image.load('Walk001.png'), pygame.image.load('Walk002.png'),
#              pygame.image.load('Walk003.png'), pygame.image.load('Walk004.png'),
#              pygame.image.load('Walk005.png'), pygame.image.load('Walk006.png'),
#              pygame.image.load('Walk007.png'), pygame.image.load('Walk008.png'),
#              pygame.image.load('Walk009.png')]
#
# walkLeft = [pygame.image.load('WalkBack000.png'),
#             pygame.image.load('WalkBack001.png'), pygame.image.load('WalkBack002.png'),
#             pygame.image.load('WalkBack003.png'), pygame.image.load('WalkBack004.png'),
#             pygame.image.load('WalkBack005.png'), pygame.image.load('WalkBack006.png'),
#             pygame.image.load('WalkBack007.png'), pygame.image.load('WalkBack008.png'),
#             pygame.image.load('WalkBack009.png')]

playerStand = pygame.image.load('car2.png')

bg = pygame.image.load('background2.jpg')   # указываем путь к заднему фону окна

clock = pygame.time.Clock()     # добавляем переменную

x = 50          # он будет распологаться в коардинатах 50
y = 375         # он будет распологаться в коардинатах 425 и не выходит за границу внизу (на верх не допрыгнет)
width = 120      # ширина игрока
height = 71     # высота нахождения игрока
speed = 5       # скорость передвижения игрока

isJump = False  # указываем прыгает игрок или нет
jumpCount = 10

left = False    # указываем что игрок не двигается влево
right = False   # указываем что игрок не двигается вправо
animCount = 0
lastMove = "right"  # последнее движение было на право для того чтобы снаряд летел туда, куда смотрел игрок


class hit():        # делаем класс для снарядов
    def __init__(self, x, y, radius, color, facing): #задаем параметры снарядов
        self.x = x  # присваеваем переменные которые относятся к данном классу
        self.y = y
        self.radius = radius
        self.color = color
        self.facing = facing
        self.vel = 8 * facing  # скорость снаряда

    def draw(self, win):    # позволяет рисовать снаряды которыми стреляем (win это то окно где рисуются снаряды)
        pygame.draw.circle(win, self.color, (self.x, self.y), self.radius) # рисуем снаряды (круглые)


def drawWindow():   # рисование на окне
    global animCount
    win.blit(bg, (0, 0))    # фон должен появляться раньше чем игрок, разрисовываем изображение

    if animCount + 1 >= 30:  # 30 кадров в сек
        animCount = 0

    if left:            # анимируем нашего персонажа
        win.blit(walkLeft[animCount // 5], (x, y))  # если
        animCount += 1
    elif right:
        win.blit(walkRight[animCount // 5], (x, y))
        animCount += 1
    else:
        win.blit(playerStand, (x, y))   # если он не двигается ни в лево ни в право он стоит на месте

    for bullet in bullets:  # перебираем весь наш массив
        bullet.draw(win)    # для каждого снаряда используем метод draw передаем в него окно в котором рисуем (win)

    pygame.display.update()  # позволяет обновлять окно

# Делаем циклы
run = True  # создаем переменную run
bullets = []  # это будет список объектов класса хит
while run:      # пока переменная True она будет выполняться бессконечно  а когда она станет False мы будем выходить
    clock.tick(30)  # указываем сколько кадров в секунду будет происходить

    for event in pygame.event.get():      # создаем переменную event (название люббое можо)
        if event.type == pygame.QUIT:
            run = False

    for bullet in bullets:  # цикл для движения и исчезновения снарядов
        if bullet.x < 500 and bullet.x > 0:  # проверяем координату снарядов двигаем снаряд
            bullet.x += bullet.vel          # до тех пор пока он не вылетит за окно
        else:
            bullets.pop(bullets.index(bullet)) # если снаряд вылетел за пределы окна значит его можно удалить

    keys = pygame.key.get_pressed()  # создаем список в котором все кнопки которые нажимаем

    if keys[pygame.K_f]:        # нажимая f стреляет снаряд
        if lastMove == "right":
            facing = 1  # если посл даиж было направо то стреляет на право
        else:
            facing = -1  # # если посл даиж было налево то стреляет налево

        if len(bullets) < 5:  # если длинна нашего списка меньше чем 5 элементов то в окне будет 5 снарядов
            bullets.append(hit(round(x + width // 1), round(y + height // 1.42), 5, (255, 0, 0), facing))
    # берем координату x где игрок по х и добавляем ширину игрока и делем ее чтобы корректировать вылет снаряда и т.д
    # 5 это радиус куда будет лететь пуля (размер ее), и цвет (тут красный), facing это куда смотрит игрок для выстрела

    if keys[pygame.K_LEFT] and x > 5:   # ходить влево тут еще указано что бы не заходил за поле окна x > 5
        x -= speed  # каждый раз когда мы будем нажимать кнопку и держать ее игрок перемещается на 5 пикселей
        left = True     # когда мы двигаемся влево то мы не двигаемся вправо
        right = False
        lastMove = "left"  # когда игрок стоит на месте он стрельнит туда куда смотрел ранее (на лево тут)
    elif keys[pygame.K_RIGHT] and x < 500 - width - 5:  # указано что бы не заходил за поле окна x > 5 до 500
        x += speed  # каждый раз когда мы будем нажимать кнопку и держать ее игрок перемещается на 5 пикселей
        left = False
        right = True    # когда мы двигаемся вправо то мы не двигаемся влево
        lastMove = "right"  # когда игрок стоит на месте он стрельнит туда куда смотрел ранее (на право тут)
    else:
        left = False    # когда мы не двигаемя ни в какую из сторон они будут False
        right = False   # когда мы не двигаемя ни в какую из сторон они будут False
        animCount = 0   # каждый раз при начале хождения в стороны анимация проигрывается с нуля (сначала)
    if not (isJump):    # указываем если мы сейчас не прыгаем тогда мы можем передвигать игрока (вправо и влево)
        if keys[pygame.K_SPACE]:    # указываем нажат ли пробел для прыжка
            isJump = True           # указываем что при нажатии на пробел он прыгал
    else:                       # указываем прыжок потом замедляемся и падаем обратно
        if jumpCount >= -10:    # мы будем прыгать вверх и вниз
            if jumpCount < 0:   # указываем чтобы игрок возвращался на землю
                y += (jumpCount ** 2) / 2   # указываем прыжок вверх
            else:
                y -= (jumpCount ** 2) / 2   # уменьшаем скорость падения
            jumpCount -= 1                  # указываем падение после прыжка
        else:
            isJump = False      # указываем что закончили прыжок
            jumpCount = 10

    drawWindow()    # вызываем нашу функцию в конце цикла

pygame.quit()   # закроет приложение после выхода из цикла